#!/usr/bin/env bash
set -e

SCRIPTS_PATH=$(realpath "$(dirname "${BASH_SOURCE[0]}")")

# Filter argument passed by control.sh
while [ $# -gt 0 ]
do
    key="$1"
    case $key in
        -h|-d)
            BACKUP_OPTS+=("${key}")
            ;;
    esac
shift # past argument or value
done

# Backups timestamp is normally supplied by control script
if [ -z "${BACKUPS_TIMESTAMP}" ]; then
    BACKUPS_TIMESTAMP=$(date +%Y%m%dT%H%M)
fi

BACKUPS_TIMESTAMP=${BACKUPS_TIMESTAMP} "${SCRIPTS_PATH}"/matomo-control.sh backup "${BACKUP_OPTS[@]}"
