#!/usr/bin/env bash
set -e

SCRIPTS_PATH=$(realpath "$(dirname "${BASH_SOURCE[0]}")")

# Filter argument passed by control.sh
while [ $# -gt 0 ]
do
    key="$1"
    case $key in
        -h|-d)
            RESTORE_OPTS+=("${key}")
            ;;
    esac
shift # past argument or value
done

"${SCRIPTS_PATH}"/matomo-control.sh restore "${RESTORE_OPTS[@]}"