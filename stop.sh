#!/usr/bin/env bash
set -e

MATOMO_HOST=fpm_matomo
RMCONFIG=0

container_is_running ( ) {
    if ! (cd "$1" && docker-compose ps "$2" |grep -q "Up "); then
        return 1
    else
        return 0
    fi
}

# Filter argument passed by control.sh
while [ $# -gt 0 ]
do
    key="$1"
    case $key in
        -V)
           COMPOSE_CMD_ARGS+="-v"
           ;;
        -C)
           RMCONFIG=1
           ;;
    esac
shift # past argument or value
done

if [ ${RMCONFIG} -eq 1 ]; then
    if container_is_running . ${MATOMO_HOST}; then
        echo -n "Removing current configuration file... "
        docker-compose exec -T $MATOMO_HOST bash -c "rm /var/www/matomo/config/config.ini.php"
        echo "done!"
    else
        echo "Cannot remove Matomo configuration file. '$MATOMO_HOST' container is not running."
    fi
fi
echo "Stopping... "
read -r -a opts_array < <(echo "${COMPOSE_OPTS}")
read -r -a args_array < <(echo "${COMPOSE_CMD_ARGS}")
cd "${COMPOSE_DIR}/clarin" &&
docker-compose "${opts_array[@]}" down "${args_array[@]}"
