#!/usr/bin/env bash
set -e

SCRIPTS_PATH=$(realpath "$(dirname "${BASH_SOURCE[0]}")")

TEST=0


# Filter argument passed by control.sh
while [ $# -gt 0 ]
do
    key="$1"
    case $key in
        -V|-R|-h|-v)
            START_OPTS+=("${key}")
            ;;
        --foreground)
            TEST=1
            ;;
    esac
shift # past argument or value
done

if [ ${TEST} -eq 1 ]; then
    read -r -a opts_array < <(echo "${COMPOSE_OPTS}")
    read -r -a args_array < <(echo "${COMPOSE_CMD_ARGS}")
    cd "${COMPOSE_DIR}/clarin" &&
    docker-compose "${opts_array[@]}" up "${args_array[@]}"
else
    "${SCRIPTS_PATH}"/matomo-control.sh start "${START_OPTS[@]}"
fi