#!/usr/bin/env bash

# This script was created before the generic CLARIN control.sh script and its corresponding API.
# The final idea is to get rid of it, migrating all its functionality to start.sh, stop.sh, 
# backup.sh, restore.sh and custom.sh (for remote sync functionality)
# Currently only stop.sh has been migrated and the other scripts are mere stubs calling this script

set -e

shopt -s expand_aliases
# shellcheck disable=SC1090
[[ -f ~/.bashrc ]] && source ~/.bashrc

MATOMO_HOST=fpm_matomo
BACKUPS_DIR=backups
DB_BACKUPS_DIR="../../${BACKUPS_DIR}/database"
WWW_BACKUPS_DIR="../../${BACKUPS_DIR}/htdocs"

STOP=0
START=0
SYNC_REMOTE=0
BACKUPDB=0
BACKUPWWW=0
RESTOREWWW=0
RESTORE=0
APPLY_UPDATE=0
RMCONFIG=0
VOLUME=0
HELP=0
VERBOSE=0
ERROR=0

container_is_running ( ) {
    if ! (cd "$1" && docker-compose ps "$2" |grep -q "Up \|running "); then
        return 1
    else
        return 0
    fi
}

container_is_healthy ( ) {
    # $1 : Docker compose file location
    # $2 : Service name
    if ! (cd "$1" && docker-compose ps "$2" |grep -q "(healthy)"); then
        return 1
    else
        return 0
    fi
}

get_container_name ( ) {
    # $1 : Docker compose file location
    # $2 : Service name
    (cd "$1"
    docker-compose images "$2" | grep "$2" | awk '{ print $1 }')
}

if [ $# -eq 0 ]; then
    echo "No option supplied!"
    HELP=1
else
    while [ $# -gt 0 ]
    do
    key="$1"
    case $key in
        stop)
            STOP=1
            ;;
        start)
            START=1
            ;;
        apply-update)
            APPLY_UPDATE=1
            START=1
            ;;
        restart)
            STOP=1
            START=1
            ;;
        backup)
            BACKUPWWW=1
            BACKUPDB=1
            ;;
        backup-db)
            BACKUPDB=1
            ;;
        backup-www)
            BACKUPWWW=1
            ;;
        sync-remote)
            #TODO review
            if [ -z "$2" ] && grep -q ':' <<<"$2"; then
                echo "Please provide an scp location <server>:<path>"
                HELP=1
                break
            else
                SCP_ADDR="$2"
            fi
            SYNC_REMOTE=1
            START=1
            RESTORE=1
            RESTOREWWW=1
            VOLUME=1
            shift
            ;;
        restore)
            RESTORE=1
            START=1
            RESTOREWWW=1
            VOLUME=1
            ;;
        -C|--del-config)
            RMCONFIG=1
            ;;
        -V|--del-volumes)
            VOLUME=1
            ;;
        -R|--restore-www)
            RESTOREWWW=1
            ;;
        -h|--help)
            HELP=1
            ;;
        -v|--debug)
            VERBOSE=1
            ;;
        *)
            echo "Unkown option: $key"
            HELP=1
            ERROR=1
            ;;
    esac
    shift # past argument or value
    done
fi

# Print parameters if running in verbose mode
if [ ${VERBOSE} -eq 1 ]; then
    set -x
fi

#
# Execute based on mode argument
#
main() {
    if [ ${HELP} -eq 1 ]; then
        echo ""
        echo "matomo-control.sh [start|stop|restart|backup-db|backup-www|backup-all|restore-remote <scp>|upgrade <TAG>] [-VRhd]"
        echo ""
        echo "  start [-VR]             Start Matomo"
        echo "  apply-update            Start Matomo after a deployment update"
        echo "  stop [-VC]              Stop Matomo"
        echo "  restart [-VRC]          Restart Matomo"
        echo "  backup                  Backup Matomo database and web files directory"
        echo "  backup-db               Backup Matomo database to backups/database/"
        echo "  backup-www              Backup Matomo web files directory to backups/htdocs/"
        echo "  sync-remote <scp>       Syncronizes this instance's database and web files with the ones from the"
        echo "                           specified scp location <scp> in the form <server>:<path>"
        echo "  restore                 Restores the most recent backup files from backups/database/ and backups/htdocs/"
        echo "                            -> If currently running restores the backup on-the-fly"
        echo ""
        echo "  -C, --del-config [stop|restart]             Regenerate Matomo configuration while starting. Implies 'stop'. Container must be running '${MATOMO_HOST}'"
        echo "  -V, --del-volumes [start|stop|restart]      Remove all Matomo volumes. Implies '-C' and 'stop'"
        echo "                                                -> Resets web files to image defaults and restores most recent database backup."
        echo "  -R, --restore-www [start|stop|restart]      Restore most recent backup from <backups/database/> before startup."
        echo "                                                -> If restoring a new version of www the database will be upgraded."
        echo "                                                -> If used together with -V restores a full backup during a restart process."
        echo "  -v, --debug             Run in verbose mode"
        echo ""
        echo "  -h, --help              Show help"
        echo ""
        if [ ${ERROR} -eq 0 ]; then
            exit 0
        else
            exit 1
        fi
    else
        if [ ${VOLUME} -eq 1 ]; then
            STOP=1
        fi
        if [ ${RMCONFIG} -eq 1 ]; then
            STOP=1
        fi

        COMPOSE_DIR="clarin"
        if readlink "$0"  >/dev/null 2>&1; then
            COMPOSE_DIR=$(dirname "$(readlink "$0")")/$COMPOSE_DIR
        else
            COMPOSE_DIR=$(dirname "${BASH_SOURCE[0]}")/$COMPOSE_DIR
        fi

        export "$(grep "COMPOSE_PROJECT_NAME" "${COMPOSE_DIR}/.env")"

        CONTAINER_NAME=$(get_container_name "${COMPOSE_DIR}" "${MATOMO_HOST}")

        cd "$COMPOSE_DIR"/

        # Backups timestamp is normally supplied by control script
        if [ -z "${BACKUPS_TIMESTAMP}" ]; then
            BACKUPS_TIMESTAMP=$(date +%Y%m%dT%H%M)
        fi
        DATE="${BACKUPS_TIMESTAMP}"

        if [ ${BACKUPDB} -eq 1 ]; then
            container_is_healthy . ${MATOMO_HOST} || (echo -n "ERROR: " ;tput sgr0; echo "Service \"$MATOMO_HOST\" is not running" >&2 && exit 1)
            echo "Backing up database... "

            docker-compose exec -T $MATOMO_HOST bash -c \
            "php console core:archive"
            # shellcheck disable=SC2016
            mysql_options='--skip-lock-tables --single-transaction --events -h ${MATOMODB_HOST} -u${MATOMODB_USER} -p${MATOMODB_PASSWORD} ${MATOMODB}'
            docker-compose exec -T $MATOMO_HOST bash -c \
            "mysqldump ${mysql_options} | gzip -9 > /tmp/matomo_db.sql.gz"
            mkdir -p "${DB_BACKUPS_DIR}"
            docker cp "${CONTAINER_NAME}:/tmp/matomo_db.sql.gz" "${DB_BACKUPS_DIR}/matomo_db_${DATE}.sql.gz"
            
            cp "${DB_BACKUPS_DIR}/matomo_db_${DATE}.sql.gz" "${DB_BACKUPS_DIR}/tmp_reown.sql.gz" \
            && mv -f "${DB_BACKUPS_DIR}/tmp_reown.sql.gz" "${DB_BACKUPS_DIR}/matomo_db_${DATE}.sql.gz"
            chmod 644 "${DB_BACKUPS_DIR}/matomo_db_${DATE}.sql.gz"
            rm -f "${DB_BACKUPS_DIR}/matomo_db.sql.gz"
            ln -s "matomo_db_${DATE}.sql.gz" "${DB_BACKUPS_DIR}/matomo_db.sql.gz"
            docker-compose exec -T $MATOMO_HOST bash -c \
            "rm -f /tmp/matomo_db.sql.gz"
            echo "Database backup successfuly saved to: ${DB_BACKUPS_DIR}/matomo_db_${DATE}.sql.gz"

        fi
        if [ ${BACKUPWWW} -eq 1 ]; then
            container_is_healthy . ${MATOMO_HOST} || (echo -n "ERROR: " ;tput sgr0; echo "Service \"$MATOMO_HOST\" is not running" >&2 && exit 1)
            echo "Backing up web files... "

            # For versions of $MATOMO_HOST image =<1.0.11
            docker-compose exec -T $MATOMO_HOST bash -c "
                which xz || (apk update && apk add xz)
                [[ -f /app/init.sh ]] && mkdir -p /init && cp -p /app/init.sh /init/docker-alpine-fpm-matomo-init.sh
                exit 0" \
            || echo "WARNING: The docker exec command returned an error"

            docker-compose exec -T $MATOMO_HOST bash -c "XZ_OPT='-T2' tar Jcf$([[ ${VERBOSE} -eq 1 ]] && echo -n 'v' || echo -n '') /tmp/matomo_www.tar.xz /var/www/matomo"
            mkdir -p "${WWW_BACKUPS_DIR}"
            docker cp "${CONTAINER_NAME}:/tmp/matomo_www.tar.xz" "${WWW_BACKUPS_DIR}/matomo_www_${DATE}.tar.xz"
            
            cp "${WWW_BACKUPS_DIR}/matomo_www_${DATE}.tar.xz" "${WWW_BACKUPS_DIR}/tmp_reown.tar.xz" \
            && mv -f "${WWW_BACKUPS_DIR}/tmp_reown.tar.xz" "${WWW_BACKUPS_DIR}/matomo_www_${DATE}.tar.xz"
            chmod 644 "${WWW_BACKUPS_DIR}/matomo_www_${DATE}.tar.xz"
            rm -f "${WWW_BACKUPS_DIR}/matomo_www.tar.xz"
            ln -s "matomo_www_${DATE}.tar.xz" "${WWW_BACKUPS_DIR}/matomo_www.tar.xz"
            docker-compose exec -T $MATOMO_HOST bash -c \
            "rm -f /tmp/matomo_www.tar.xz"
            echo "Matomo application directory backup successfuly saved to: ${WWW_BACKUPS_DIR}/matomo_www_${DATE}.tar.xz"
        fi
        if [ ${SYNC_REMOTE} -eq 1 ]; then
            echo "Syncronizing data and web files with running instance on $SCP_ADDR"
            # shellcheck disable=SC2206
            arrIN=(${SCP_ADDR//:/ })
            SCP_SERVER="${arrIN[0]}"
            SCP_PATH="${arrIN[1]}"
            HOSTNAME=$(hostname)

            # Generate temporary backups remotely
            (cd ..
            echo "Creating a fresh remote backup..."
            # shellcheck disable=SC2016
            mysql_options='--skip-lock-tables --single-transaction --default-character-set=utf8mb4 --events -h \\\${MATOMODB_HOST} -u\\\${MATOMODB_USER} -p\\\${MATOMODB_PASSWORD} \\\${MATOMODB}'
            SSH_COMMAND="bash -c \"
                cd ${SCP_PATH}/$(basename "$(pwd)")/clarin
                docker-compose exec -T $MATOMO_HOST bash -c \\\"
                    php console core:archive
                    mysqldump ${mysql_options} | gzip -9 > /tmp/matomo_db.sql.gz

                    which xz || (apk update && apk add xz)
                    [[ -f /app/init.sh ]] && mkdir -p /init && cp -p /app/init.sh /init/docker-alpine-fpm-matomo-init.sh
    
                    XZ_OPT='-T2' tar Jcf$([[ ${VERBOSE} -eq 1 ]] && echo -n 'v' || echo -n '') /tmp/matomo_www.tar.xz /var/www/matomo
                \\\"
                REMOTE_CONTAINER_NAME=\\\$(docker-compose images ${MATOMO_HOST} | grep ${MATOMO_HOST} | awk '{ print \\\$1 }')
                docker cp \\\${REMOTE_CONTAINER_NAME}:/tmp/matomo_db.sql.gz ${SCP_PATH}/${DB_BACKUPS_DIR}/matomo_db_-${HOSTNAME}-_${DATE}.sql.gz
                docker cp \\\${REMOTE_CONTAINER_NAME}:/tmp/matomo_www.tar.xz ${SCP_PATH}/${WWW_BACKUPS_DIR}/matomo_www_-${HOSTNAME}-_${DATE}.tar.xz
                exit
                \"
            "
            ssh "$SCP_SERVER" -tt "$SSH_COMMAND")
            echo "Remote backups successfuly created. Fetching them..."

            scp "${SCP_SERVER}:${SCP_PATH}/${DB_BACKUPS_DIR}/matomo_db_-${HOSTNAME}-_${DATE}.sql.gz" ${DB_BACKUPS_DIR}
            scp "${SCP_SERVER}:${SCP_PATH}/${WWW_BACKUPS_DIR}/matomo_www_-${HOSTNAME}-_${DATE}.tar.xz" ${WWW_BACKUPS_DIR}
            rm -f "${DB_BACKUPS_DIR}/matomo_db.sql.gz" "${WWW_BACKUPS_DIR}/matomo_db.tar.xz"
            ln -s "matomo_db_-${HOSTNAME}-_${DATE}.sql.gz" "${DB_BACKUPS_DIR}/matomo_db.sql.gz"
            ln -s "matomo_www_-${HOSTNAME}-_${DATE}.tar.xz" "${WWW_BACKUPS_DIR}/matomo_db.tar.xz"
        fi
        if [ ${RESTORE} -eq 1 ]; then
            # If already running syncronize on-the-fly
            if container_is_healthy . ${MATOMO_HOST}; then
                VOLUME=0
                STOP=0
            fi
        fi
        if [ ${STOP} -eq 1 ]; then
            if [ ${RMCONFIG} -eq 1 ]; then
                stop_args+="-C"
            fi
            if [ ${VOLUME} -eq 1 ]; then
                stop_args+="-V"
            fi
            echo "Stopping... "
            COMPOSE_DIR=".." ../stop.sh "${stop_args[@]}"
        fi
        if [ ${APPLY_UPDATE} -eq 1 ]; then
            echo -n "Preparing to start with updated version... "
            VOLUMENAME="${COMPOSE_PROJECT_NAME}_matomo_app_dir"
            docker volume rm "${VOLUMENAME}"  && echo "Done." \
            || echo "WARNING: volume ${VOLUMENAME} not found. No volume removed!"
            echo "Done."
        fi
        if [ ${START} -eq 1 ]; then
            echo "Starting... "
            # Create postfix_mail network if does exist
            if [ "$(docker network ls |grep -c postfix_mail)"  -eq 0 ]; then
                docker network create postfix_mail
            fi
            if [ ${VERBOSE} -eq 1 ]; then
                # While debugging run docker compose undaemonized but in a child shell
                bash -c "cd $(pwd); docker-compose up &"
            else
                docker-compose up -d
            fi

            if [ ${RESTOREWWW} -eq 1 ]; then

                while ! container_is_running . ${MATOMO_HOST}
                    do
                    echo "Waiting for ${MATOMO_HOST} container to become available"
                    sleep 5
                done
                
                if [ ${VOLUME} -eq 1 ]; then
                    echo -n "Saving current config.ini.php file... "
                    docker-compose exec -T $MATOMO_HOST bash -c \
                    "cp config/config.ini.php /tmp/config.ini.php.bak"
                    echo "Done."
                fi

                # shellcheck disable=SC2012
                BACKUPFILE=${WWW_BACKUPS_DIR}/$(basename "$(ls -Fat ${WWW_BACKUPS_DIR}/matomo_www_*.tar.xz | head -1)")
                echo "Restoring file $BACKUPFILE... "
                docker cp "$BACKUPFILE"  "${CONTAINER_NAME}:/tmp/matomo_www.tar.xz"

                # For versions of $MATOMO_HOST image =<1.0.11
                docker-compose exec -T $MATOMO_HOST bash -c "
                    which xz || (apk update && apk add xz)
                    [[ -f /app/init.sh ]] && mkdir -p /init && cp -p /app/init.sh /init/docker-alpine-fpm-matomo-init.sh
                    exit 0" \
                    || echo "WARNING: The docker exec command returned an error"

                docker-compose exec -T $MATOMO_HOST bash -c \
                "XZ_OPT='-T2' tar Jxf$([[ ${VERBOSE} -eq 1 ]] && echo -n 'v' || echo -n '') /tmp/matomo_www.tar.xz -C /
                rm -f /tmp/matomo_www.tar.xz"
                
                if [ ${VOLUME} -eq 1 ]; then
                    echo -n "Restoring old config.ini.php file... "
                    docker-compose exec -T $MATOMO_HOST bash -c \
                    "cp /tmp/config.ini.php.bak config/config.ini.php"
                    echo "Done"
                fi

                echo "Done."

                if [ ${RESTORE} -eq 1 ] && [ ${VOLUME} -eq 0 ]; then

                    echo "Restoring database backup..."
                    while ! container_is_healthy . ${MATOMO_HOST}
                    do
                        echo "Waiting for ${MATOMO_HOST} container to become available"
                        sleep 5
                    done

                    # Set maintenance mode
                    echo -n "Setting maintenance mode... "
                    docker-compose exec -T $MATOMO_HOST bash -c \
                    "sed -i '/\\[General\\]/a maintenance_mode = 1' config/config.ini.php
                    sed -i '/record_statistics = /s/^/;/g' config/config.ini.php
                    sed -i '/\\[Tracker\\]/a record_statistics = 0' config/config.ini.php"
                    echo "Done."

                    #restore database
                    # shellcheck disable=SC2012
                    BACKUPFILE="${DB_BACKUPS_DIR}/$(basename "$(ls -Fat ${DB_BACKUPS_DIR}/matomo_db_*.sql.gz | head -1)")"
                    echo -n "Restoring database file: ${BACKUPFILE} ..."
                    docker cp "$BACKUPFILE"  "${CONTAINER_NAME}:/tmp/matomo_db.sql.gz"
                    # shellcheck disable=SC2016
                    mysql_options='-h ${MATOMODB_HOST} -u${MATOMODB_USER} -p${MATOMODB_PASSWORD} --default-character-set=utf8mb4'
                    docker-compose exec -T $MATOMO_HOST bash -c \
                    "(echo \"DROP DATABASE IF EXISTS \${MATOMODB};CREATE DATABASE \${MATOMODB};\" | mysql ${mysql_options}) && (gunzip -c /tmp/matomo_db.sql.gz | mysql ${mysql_options} \${MATOMODB})
                    rm -f /tmp/matomo_db.sql.gz"
                    echo "Done."

                    # Uset maintenance mode
                    echo -n "Unsetting maintenance mode... "
                    docker-compose exec -T $MATOMO_HOST bash -c \
                    "sed -i '/maintenance_mode = 1/d' config/config.ini.php
                    sed -i '/^record_statistics = 0/d' config/config.ini.php
                    sed -i '/record_statistics = /s/^;//g' config/config.ini.php"
                    echo "Done."

                fi
                if [ ${VOLUME} -eq 0 ]; then
                    echo -n "Running ${MATOMO_HOST} container initialization sequence... "
                    docker-compose exec -T "$MATOMO_HOST" bash -c \
                    "rm config/config.ini.php 
                    . /init/docker-alpine-fpm-matomo-init.sh"
                    echo "Done."
                fi
            fi
            while ! container_is_healthy . ${MATOMO_HOST}
                do
                    echo "Waiting for ${MATOMO_HOST} container to become available"
                    sleep 5
            done
        fi
    fi
}
main "$@"; exit
